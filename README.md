# job_tracker

A command line script that aids in tracking jobs by their status.
It stores jobs in a .csv file by number and description. It allows
adding, deleting, archiving, changing, and reporting job status.
The .csv format is:

job_number,description,status

## Files

The script writes files into the directory from which it is run.
- Jobs database: jobfile.csv
- Jobs archive: jobarchive.csv
- Backup file: jobfile.csv~
- A temporary file: temp

## Help
`python job_tracker.py -h`
