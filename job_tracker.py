# job_tracker.py
# Marcel Brown
# 2018-12-17

# A simple job tracking script that stores jobs in a .csv file by number and
# description. It allows adding, deleting, archiving, changing, and reporting
# job status.

# @TODO: generally make this terrible code better
# @TODO: make multiple arguments error-safe (i.e.: require correct number
#        for functions where required.

# IMPORTS
import csv
#import pprint
from argparse import ArgumentParser


# DEFINE CONSTANTS
jobfile = "jobfile.csv"
archivefile = "jobarchive.csv"
backupfile = "jobfile.csv~"
tempfile = "temp"


# DEFINE FUNCTIONS
def start_job(job_number, job_description):
    '''Adds a job to the list with a default status of "opened."'''
    job_status = "opened"
    with open(jobfile, "r+", newline='') as job_file:
        filereader = csv.reader(job_file, delimiter=',')
        filewriter = csv.writer(job_file, delimiter=',')
        for row in filereader:
            if job_number in row:
                return "exists"
        if job_number:
            filewriter.writerow([job_number, job_description, job_status])
        else:
            return "no_number"
    return [job_number, job_description, job_status]


def backup_job():
    '''Overwrites a persistent backup file with the current job file.'''
    with open(jobfile, "r", newline='') as job_file, open(backupfile, "w", newline='') as backup_file:
        filereader = csv.reader(job_file, delimiter=',')
        filewriter = csv.writer(backup_file, delimiter=',')
        for row in filereader:
            filewriter.writerow(row)
    return "success"


def does_job_exist(job_to_check):
    '''Check every line in a .csv file for the specified string.
       Returns boolean.'''
    with open(jobfile, "r", newline='') as job_file:
        does_job_exist = False
        filereader = csv.reader(job_file, delimiter=',')
        for row in filereader:
            if job_to_check in row:
                does_job_exist = True
        return does_job_exist


def delete_job(job_to_delete):
    '''Deletes a job by writing all but the deleted job rows to a temporary
       file and then overwriting the original file with the temporary file.'''
    # @TODO: this is /absurd/ - there must be a better way
    check_exist = does_job_exist(job_to_delete)
    if check_exist is False:
        return "job_does_not_exist"
    with open(jobfile, "r", newline='') as job_file, open(tempfile, "w", newline='') as temp_file:
        filereader = csv.reader(job_file, delimiter=',')
        filewriter = csv.writer(temp_file, delimiter=',')
        for row in filereader:
            if row[0] != job_to_delete:
                filewriter.writerow(row)
    with open(tempfile, "r", newline='') as temp_file, open(jobfile, "w", newline='') as job_file:
        filereader = csv.reader(temp_file, delimiter=',')
        filewriter = csv.writer(job_file, delimiter=',')
        for row in filereader:
            filewriter.writerow(row)
    return job_to_delete


def archive_job(job_to_archive):
    '''Archives a job to a separate file by appending it's row.
       Requires delete_job.'''
    success = False
    with open(jobfile, "r", newline='') as job_file, open(archivefile, "a", newline='') as archive_file:
        filereader = csv.reader(job_file, delimiter=',')
        filewriter = csv.writer(archive_file, delimiter=',')
        for row in filereader:
            if row[0] == job_to_archive:
                filewriter.writerow(row)
                success = True
    if success is True:
        deleted_archived_job = delete_job(job_to_archive)
        return deleted_archived_job


def change_status(job_to_change, new_status):
    '''Change the status of a job.'''
    row_updated = False
    with open(jobfile, "r", newline='') as job_file:
        filereader = csv.reader(job_file, delimiter=',')
        for row in filereader:
            if row[0] == job_to_change:
                updated_row = [row[0], row[1], new_status]
                row_updated = True
    if row_updated is True:
        delete_job(job_to_change)
        with open(jobfile, "a", newline='') as job_file:
            filewriter = csv.writer(job_file, delimiter=',')
            filewriter.writerow(updated_row)
            return [job_to_change, new_status]
    else:
        return "not_found"


def query_by_number(job_number):
    '''Returns job information by job number.'''
    with open(jobfile, "r", newline='') as job_file:
        filereader = csv.reader(job_file, delimiter=',')
        for row in filereader:
            if job_number in row:
                return row


def query_status():
    '''Creates a report listing jobs grouped by status.'''
    with open(jobfile, "r", newline='') as job_file:
        filereader = csv.reader(job_file, delimiter=',')
        statuses = []
        report = {}
        for row in filereader:
            statuses.append(row[2])
            # remove dupes
            statuses = set(statuses)
            statuses = list(statuses)
            for status in statuses:
                if status in row:
                    # check whether status has already been added to report
                    if status in report:
                        # append as a list to the existing values
                        report[status].append([row[0], row[1]])
                    else:
                        # make key and value pair (list within list
                        # for consistency with the above append)
                        report[status] = [[row[0], row[1]]]
    return report

# DEFINE MAIN


def main():
    '''The main job tracker function, including command line arguments.'''
    # DEFINE ARGUMENT PARSER
    # note: the -h argument is automatic
    parser = ArgumentParser()
    parser.add_argument("options", nargs="*", help="job number, description/new job status")
    parser.add_argument("-n", "--new", help="Enter a new job", action="store_true")
    parser.add_argument("-d", "--delete", help="Delete a job", action="store_true")
    parser.add_argument("-a", "--archive", help="Archive a job", action="store_true")
    parser.add_argument("-c", "--change", help="Change job status", action="store_true")
    parser.add_argument("-q", "--query", help="Get job information", action="store_true")
    parser.add_argument("-s", "--status", help="Display status for all jobs", action="store_true")
    args = parser.parse_args()

    if args.new:
        backup_job()
        if args.options:
            job_to_add = start_job(args.options[0], args.options[1])
        else:
            job_number = input("Start a job. Enter the job number: ")
            job_description = input("Enter a job description: ")
            job_to_add = start_job(job_number, job_description)
        if job_to_add == "exists":
            print("The job number already exists.\n")
        elif job_to_add == "no_number":
            print("You must enter a job number.\n")
        else:
            print("Job {0}: {1} was added. STATUS: {2}\n".format(job_to_add[0], job_to_add[1], job_to_add[2]))

    if args.query:
        if args.options:
            query_result = query_by_number(args.options[0])
        else:
            job_number = input("Input a job number to query: ")
            query_result = query_by_number(job_number)
        if query_result:
            print("Job {0}: {1}. STATUS: {2}\n".format(query_result[0], query_result[1], query_result[2]))
        else:
            print("Job number not found.\n")

    if args.status:
        status_query = query_status()
        #pp = pprint.PrettyPrinter(indent=4)
        #pp.pprint(status_query)
        for k, v in status_query.items():
            print("  {}: ".format(k))
            for items in v:
                print("     Job {0}: {1}".format(items[0], items[1]))

    if args.delete:
        backup_job()
        if args.options:
            result = delete_job(args.options[0])
        else:
            job_to_delete = input("Enter the job number to be deleted: ")
            result = delete_job(job_to_delete)
        if result == "job_does_not_exist":
            print("The job number does not exist.")
        elif result:
            print("Job {0} has been deleted.\n".format(result))
        else:
            print("The job was not deleted (or error).\n")

    if args.archive:
        backup_job()
        if args.options:
            result = archive_job(args.options[0])
        else:
            job_to_archive = input("Enter the job number to be archived: ")
            result = archive_job(job_to_archive)
        if result:
            print("Job {0} has been archived.\n".format(result))
        else:
            print("The job was not archived (or error).\n")

    if args.change:
        backup_job()
        if args.options:
            result = change_status(args.options[0], args.options[1])
        else:
            job_to_change = input("Enter the job number to change: ")
            new_status = input("Enter the new status: ")
            result = change_status(job_to_change, new_status)
        if result == "not_found":
            print("The job number was not found.\n")
        elif result:
            print("The status of job {0} has been changed to \"{1}\".\n".format(result[0], result[1]))
        else:
            print("Error.\n")


if __name__ == '__main__':
    main()
